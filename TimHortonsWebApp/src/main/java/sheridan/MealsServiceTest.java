package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testDrinksRegular() throws Exception{
        List<String> mealType = MealsService.getAvailableMealTypes(MealType.DRINKS);
        
		assertTrue("Invalid value for mealType", (mealType != null));
	}
	
	@Test
	public void testDrinksException() throws Exception{
		MealType type = null;
        List<String> mealType = MealsService.getAvailableMealTypes(type);
        
		assertFalse("Invalid value for mealType", (mealType.get(0) != "No Brand Available"));
	}
	@Test
	public void testDrinksBoundaryIn() throws Exception{
		List<String> mealType = MealsService.getAvailableMealTypes(MealType.DRINKS);
        
		assertTrue("Invalid value for mealType", (mealType.size() > 3));
	}
	
	@Test
	public void testDrinksBoundaryOut() throws Exception{
		MealType type = null;
        List<String> mealType = MealsService.getAvailableMealTypes(type);
        
		assertFalse("Invalid value for mealType", (mealType.size() < 1));
	}

}
